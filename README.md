<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#sec-1">1. Options for securing internal components</a></li>
<li><a href="#sec-2">2. UAA integration options (summary of CORE-933)</a>
<ul>
<li><a href="#sec-2-1">2.1. server side integration</a></li>
<li><a href="#sec-2-2">2.2. client side integration options</a></li>
</ul>
</li>
<li><a href="#sec-3">3. Breakdown of integration work</a></li>
</ul>
</div>
</div>


# Options for securing internal components<a id="sec-1" name="sec-1"></a>

-   no security (or firewall-based security)
    -   components are behind firewall and exposed through director only.
    -   no authentication needed
-   2-way SSL
    -   components can be distributed independently, but not exposed independently
    -   "not a standard GE way of securing internal services"
-   Basic auth over SSL
    -   "less secure than 2-way SSL, usually frowned upon by security folks"
-   JEE-based auth
    -   "less flexible than oauth"
    -   "not a GE standard"
-   UAA
    -   implements oauth
    -   Predix standard
    -   flexible, supports fine-grained authorization
    -   allows components to be exposed to users

# UAA integration options (summary of CORE-933)<a id="sec-2" name="sec-2"></a>

## server side integration<a id="sec-2-1" name="sec-2-1"></a>

-   A set of issuers known as "trusted" are made known to the service
-   Each incoming request must be checked for a valid token
    -   token must have been issued by a trusted issuer
    -   token is not expired
    -   token has sufficient scopes for the resource requested
        -   can start with a single "super-admin" service-to-service scope,
            which can eventually be made more fine-grained based on requirements
        -   may require maintaining a map of URI resources to scopes to support
            fine-grained authorization
-   Java request-filter prototyped by Victor as part of CORE-933 (link)

## client side integration options<a id="sec-2-2" name="sec-2-2"></a>

-   forwarding authorization context
    -   does not allow inter-service calls to require different privileges from end-user authorization
-   obtaining authorization context within each template
    -   "not scalable", requiring changes to every template
-   automatic enrichment of outgoing http calls based on centralized URI registry
    -   "the kernel is modified to intercept and analyze outgoing http calls"
    -   A per-component URI registry specifying necessary credentials for each URI
        is established, allowing kernel to automatically enrich outgoing http calls that are missing
        "Authorization" information (link to example)
    -   the interception points are in HttpSource and HttpTransport classes
    -   registry is component-specific:
        "since each application using the kernel uses different remote resources,
        authorization information for each app must be limited to this app"
    -   unlike other options, minimal or no changes required on the templates. more scalable
    -   based on unverified assumption that all template http calls are made through bstools

# Breakdown of integration work<a id="sec-3" name="sec-3"></a>

1.  client-authorization framework
    -   spike to determine, for each app/components, all services that it depends on
        -   this will determine, for each app, the list of URIs and corresponding clients
        -   victor has prototyped director, director services. 
            -   router, scheduler, registry, etc pending
    -   create "client authorization framework"
        -   establish URI registry
        -   need to confirm that all http calls are made through bstools
        -   modify bstools to enable the framework (prototyped)
2.  server-side enforcement
    -   framework
        -   check token (prototyped)
        -   map endpoint URI to scopes for fine-grained authorization
            -   alternatively, start with a single hard-coded super-admin service-to-service scope
3.  integrate each application with client-authorization framework
    -   sample integration points: web.xml, bstools.xml
4.  configure customer env
    -   setup uaa, create uaa clients